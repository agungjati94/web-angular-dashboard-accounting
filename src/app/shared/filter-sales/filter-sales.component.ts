import { Component, OnInit, Input, Inject } from '@angular/core';
import FilterModel from 'src/app/model/filter.model';
import { SalesService } from 'src/app/service/sales.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { DashboardService } from 'src/app/service/dashboard.service';
import { ProcrumentService } from 'src/app/service/procrument.service';

@Component({
  selector: 'app-filter-sales',
  templateUrl: './filter-sales.component.html',
  styleUrls: ['./filter-sales.component.css']
})
export class FilterSalesComponent implements OnInit {

  public Divisions: Array<FilterModel>
  public Departments: Array<FilterModel>
  public Projects: Array<FilterModel>
  public Requestors: Array<FilterModel>
  public POTypes: Array<FilterModel>
  public Currencys: Array<FilterModel>

  @Input() Division :number;
  @Input() X :number;
  @Input() public Department : number;
  @Input() public Project : number;
  @Input() public Requestor : number;
  @Input() public POType : number;
  @Input() public Currency : number;
  @Input() public Status : number;
  statusData: any;
  IsSO : boolean
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<FilterSalesComponent>,
  private service: DashboardService, private snackBar: MatSnackBar, private serviceSetup: ProcrumentService) { }

  ngOnInit() {
    this.IsSO = ((this.data.type == "SONeedtoConfirm") || (this.data.type == "SONeedtoRunning") ||
                 (this.data.type == "OutstandingSOAgainstDO") || (this.data.type == "OutstandingSOAgainstInvoice") || 
                 (this.data.type == "NumberofSO") || (this.data.type == "OpenSOValue") || (this.data.type == "NumberofCustomer"));

    this.serviceSetup.getSetupSales(this.data.type).subscribe((respone) => {
      this.Division = respone.divisionId
      this.Department = respone.departmentId
      this.Project = respone.projectId
      this.Requestor = respone.requestorId
      this.POType = respone.potypeId
      this.Currency = respone.currencyId
      this.Status = respone.statusId
    }, error => {
      this.displayError(error);
    });
    this.service.getDivision().subscribe((respone) => {
      this.Divisions = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getDepartment().subscribe((respone) => {
      this.Departments = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getProject().subscribe((respone) => {
      this.Projects = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getRequestor().subscribe((respone) => {
      this.Requestors = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getCurrency().subscribe((respone) => {
      this.Currencys = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getStatus().subscribe(res => {this.statusData = res})

    if(this.IsSO){
      this.service.getPOType().subscribe((respone) => {
        this.POTypes = respone
      }, error => {
        this.displayError(error);
      });
    }
  }

  private displayError(error: any) {
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }

  onDone() : void {
    let param = {
      Type: this.data.type,
      DivisionId: this.Division,
      DepartmentId: this.Department,
      ProjectId: this.Project,
      RequestorId: this.Requestor,
      POTypeId: this.POType,
      CurrencyId: this.Currency,
      StatusId: this.Status
    }

    this.serviceSetup.saveFilterSales(param).subscribe(() => {
      this.dialogRef.close(true);
    }, 
      error => {
        this.displayError(error)
      })
  }

  onClose(){
    this.dialogRef.close(false);
  }
}

