import { Component, OnInit, Inject } from '@angular/core';
import { MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { ProcurementdetailComponent } from '../procurementdetail/procurementdetail.component';
import { ProcrumentService } from 'src/app/service/procrument.service';

@Component({
  selector: 'app-procurementchartdetail',
  templateUrl: './procurementchartdetail.component.html',
  styleUrls: ['./procurementchartdetail.component.css']
})
export class ProcurementchartdetailComponent implements OnInit {
  displayedColumns: string[];
  dataSource : any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
   @Inject("POURL") private POURL : string,
  public dialogRef: MatDialogRef<ProcurementdetailComponent>,
  private procrumentService: ProcrumentService,
  private dialog: MatDialog, 
  private snackBar : MatSnackBar) { }

  ngOnInit() {
    if(this.data.type == "OpenPOValue"){
      this.displayedColumns= ['no', 'cd', 'date', 'supplier', 'amount'];
    } else if(this.data.type == "NumberofSupplier"){
      this.displayedColumns= ['no', 'supplier', 'amount'];
    } else if(this.data.type =='TotalSupplier'){
      this.displayedColumns= ['no', 'name', 'phone', 'pic', 'address'];
    }

    this.dataSource = new MatTableDataSource<any>(this.data.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onGoToDetailTrx({id, vendorId, month}){
    if(this.data.type == 'NumberofSupplier'){
      this.procrumentService.GetChartDetailNumberOfActiveSupplier(month, vendorId).subscribe((data: any) => {
        this.dialog.open(ProcurementdetailComponent, {
          width: '85vw',
          height: '90vh',
          data: { data: data, type: "NumberofSupplier" }
        });
      }, error => {
        this.displayError(error);
      })
    }else if(this.data.type == 'TotalSupplier'){
      this.procrumentService.GetDetailTotalSupplierPOByCountryVendor(vendorId, 1, 20).subscribe(({ data, header }: any) => {
        this.dialog.open(ProcurementdetailComponent, {
          width: '85vw',
          height: '90vh',
          data: { data: data, header : header, vendorId: vendorId , type: "TotalSupplier" }
        });
      }, error => {
        this.displayError(error);
      })
    }
    else{
      window.open(this.POURL +"poT/"+ id, '_blank');
    }
  }

  private displayError(error: any) {
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }

}
