import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcurementchartdetailComponent } from './procurementchartdetail.component';

describe('ProcurementchartdetailComponent', () => {
  let component: ProcurementchartdetailComponent;
  let fixture: ComponentFixture<ProcurementchartdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcurementchartdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcurementchartdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
