import { Component, OnInit, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { DashboardService } from 'src/app/service/dashboard.service';
import FilterModel from 'src/app/model/filter.model';
import { ProcrumentService } from 'src/app/service/procrument.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  public Divisions: Array<FilterModel>
  public Departments: Array<FilterModel>
  public Projects: Array<FilterModel>
  public Requestors: Array<FilterModel>
  public POTypes: Array<FilterModel>
  public Currencys: Array<FilterModel>

  @Input() public Division : number;
  @Input() public Department : number;
  @Input() public Project : number;
  @Input() public Requestor : number;
  @Input() public POType : number;
  @Input() public Currency : number;
  IsPO : boolean
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<FilterComponent>,
  private router:Router,
  private service: DashboardService, private snackBar: MatSnackBar, private procurementService: ProcrumentService) { }

  ngOnInit() {
    this.IsPO = ((this.data.type == "PONeedtoConfirm") || (this.data.type == "PONeedtoRunning") ||
                 (this.data.type == "OutstandingPOAgainstGRN") || (this.data.type == "OutstandingPOAgainstSI") || 
                 (this.data.type == "NumberofPO") || (this.data.type == "OpenPOValue") || (this.data.type == "NumberofSupplier"));

    this.service.getSetupProcurement(this.data.type).subscribe((respone) => {
      this.Division = respone.divisionId
      this.Department = respone.departmentId
      this.Project = respone.projectId
      this.Requestor = respone.requestorId
      this.POType = respone.potypeId
      this.Currency = respone.currencyId
    }, error => {
      this.displayError(error);
    });
   
    this.service.getDivision().subscribe((respone) => {
      this.Divisions = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getDepartment().subscribe((respone) => {
      this.Departments = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getProject().subscribe((respone) => {
      this.Projects = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getRequestor().subscribe((respone) => {
      this.Requestors = respone
    }, error => {
      this.displayError(error);
    });
    this.service.getCurrency().subscribe((respone) => {
      this.Currencys = respone
    }, error => {
      this.displayError(error);
    });

    if(this.IsPO){
      this.service.getPOType().subscribe((respone) => {
        this.POTypes = respone
      }, error => {
        this.displayError(error);
      });
    }
  }

  private displayError(error: any) {
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }

  onDone() : void {
    let param = {
      Type: this.data.type,
      DivisionId: this.Division,
      DepartmentId: this.Department,
      ProjectId: this.Project,
      RequestorId: this.Requestor,
      POTypeId: this.POType,
      CurrencyId: this.Currency
    }

    this.procurementService.saveFilterProcurement(param).subscribe(() => {
      this.dialogRef.close(true);
    }, 
      error => {
        this.displayError(error)
      })
  }

  onClose(){
    this.dialogRef.close(false);
  }
}

