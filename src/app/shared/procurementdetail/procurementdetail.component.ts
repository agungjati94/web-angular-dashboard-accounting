import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource, MatSnackBar, MatPaginator } from '@angular/material';
import { ProcrumentService } from 'src/app/service/procrument.service';

@Component({
  selector: 'app-procurementdetail',
  templateUrl: './procurementdetail.component.html',
  styleUrls: ['./procurementdetail.component.css']
})
export class ProcurementdetailComponent implements OnInit {
  displayedColumns: string[] = ['no', 'cd', 'requestor', 'date', 'division', 'department', 'project', 'status'];
  dataSource : MatTableDataSource<any>;
  IsPO : boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  Limit: number = 20;
  TotalRow: any;
  onLoading: boolean;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
   @Inject("MPRURL") private MPRURL : string,
   @Inject("POURL") private POURL : string,
   private procrumentService: ProcrumentService,
   private snackBar : MatSnackBar,
   public dialogRef: MatDialogRef<ProcurementdetailComponent>) { }

  ngOnInit() {
    this.IsPO = ((this.data.type == "PONeedtoConfirm") || (this.data.type == "PONeedtoRunning") ||
    (this.data.type == "OutstandingPOAgainstGRN") || (this.data.type == "OutstandingPOAgainstSI") ||
    (this.data.type == "NumberofPO") || (this.data.type == "NumberofSupplier") || (this.data.type == "TotalSupplier"));
    this.dataSource = new MatTableDataSource<any>(this.data.data)
    this.TotalRow = (this.data.header && this.data.header.total) || 0
    this.dataSource.paginator = this.paginator;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onGoToDetailTrx(id){
    if(this.IsPO){
      window.open(this.POURL +"/poT/"+ id, '_blank');
    }else {
      window.open(this.MPRURL +"/MPRT/"+ id, '_blank');
    }
  }

  onMovePage(e){
    this.onLoading = true
    this.procrumentService.GetDetailTotalSupplierPOByCountryVendor(this.data.vendorId, ++e.pageIndex, this.Limit).subscribe(({data, header } : any) => {
      this.TotalRow = header.total
      this.dataSource = new MatTableDataSource<any>(data)
      this.onLoading = false
    }, error => {
      this.displayError(error);
    })
  }

  private displayError(error: any) {
    this.onLoading = false
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }

}
