import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcurementdetailComponent } from './procurementdetail.component';

describe('ProcurementdetailComponent', () => {
  let component: ProcurementdetailComponent;
  let fixture: ComponentFixture<ProcurementdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcurementdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcurementdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
