import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShowhidemenuService {

  private showMenu = new BehaviorSubject<boolean>(true);
  currentShowMenu = this.showMenu.asObservable();
  
  constructor() { }

  changeShowMenu(showmenu: boolean) {
    this.showMenu.next(showmenu);
  }
}
