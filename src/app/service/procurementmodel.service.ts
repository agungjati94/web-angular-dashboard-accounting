import { Injectable } from '@angular/core';

@Injectable()
export class ProcurementModel {
  countMPRNeedToConfirm: number = 0;
  countMPRNeedToRunning: number = 0;
  countOutstandingMPRAgainstPO: number = 0;
  countOutstandingMPRAgainstGRN: number = 0;
  countPONeedtoConfirm: number = 0;
  countPONeedtoRunning: number = 0;
  countOutstandingPOAgainstGRN: number = 0;
  countOutstandingPOAgainstSI: number = 0;
  numberOfPO: any;
  numberOfMPR: any;
  openPOValue: any;
  numberOfActiveSupplier: any;
  supplier: any;
}