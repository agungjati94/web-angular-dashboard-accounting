import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkcalendarService {

  private APIURL: string = window["SofiConfig"].GatewayApi;
  constructor(private httpClient: HttpClient) { }

  GetDue(types : string[], date : string) : Observable<any>{
    let params = new HttpParams();
    types.forEach(t => {
      params = params.append('type', t)
    });
    params = params.append('date', date);

    return this.httpClient.get(this.APIURL + "/api/dashboard/GetDue", { params : params })
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetDueMPR")
      console.log(error)
    }))
  }
}
