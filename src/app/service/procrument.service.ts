import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProcrumentService {
  
 private APIURL : string = window["SofiConfig"].GatewayApi;
  constructor(private httpClient: HttpClient) { }

  CountMPRNeeedtoConfirm() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountMPRNeeedtoConfirm")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountMPRNeeedtoConfirm")
      console.log(error)
    }))
  }

  CountMPRNeeedtoRunning() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountMPRNeeedtoRunning")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountMPRNeeedtoRunning")
      console.log(error)
    }))
  }
  CountMPROutstandingAgainstPO() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountMPROutstandingAgainstPO")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountMPROutstandingAgainstPO")
      console.log(error)
    }))
  }

  GetProcurementDetail(type) : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetProcurementDetail?type=" + type)
    .pipe(map(res => {
      return res;
    }))
  }

  CountPONeeedtoConfirm() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountPONeeedtoConfirm")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountPONeeedtoConfirm")
      console.log(error)
    }))
  }
  CountPONeeedtoRunning() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountPONeeedtoRunning")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountPONeeedtoRunning")
      console.log(error)
    }))
  }
  CountPOOutstandingAgainstGRN() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountPOOutstandingAgainstGRN")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountPOOutstandingAgainstGRN")
      console.log(error)
    }))
  }
  
  CountSIOutstanding() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSIOutstanding")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountSIOutstanding")
      console.log(error)
    }))
  }

  saveFilterProcurement(param: any) : Observable<any>{
    return this.httpClient.post(this.APIURL + "/api/dashboard/SaveFilterProcurement", param)
    .pipe(map(res => {
      return res;
    },error => {
      console.log("SaveFilterdashboard")
      console.log(error)
    }))
  }

  CountPOOutstandingAgainstSI(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountPOOutstandingAgainstSI")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountPOOutstandingAgainstSI")
      console.log(error)
    }))
  }


  CountMPROutstandingAgainstGRN(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountMPROutstandingAgainstGRN")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("CountMPROutstandingAgainstGRN")
      console.log(error)
    }))
  }

  GetChartDetail(type, month): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetChartDetail?type=" + type + "&month=" + month)
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetChartDetail")
      console.log(error)
    }))
  }

  GetPONumber(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/PONumber")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("PONumber")
      console.log(error)
    }))
  }

  GetMPRNumber(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/MPRNumber")
    .pipe(map(res => {
      return res;
    }))
  }

  GetOpenPOValue(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/OpenPOValue")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("OpenPOValue")
      console.log(error)
    }))
  }

  GetNumberOfSupplier(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/NumberOfSupplier")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("NumberOfSupplier")
      console.log(error)
    }))
  }

  GetTotalSupplier(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/TotalSupplier")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("TotalSupplier")
      console.log(error)
    }))
  }

  GetChartDetailOpenPOValue(type, month, currency): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetChartDetailOpenPOValue?type=" + type + "&month=" + month + "&currencyId=" + currency)
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetChartDetail")
      console.log(error)
    }))
  }

  GetChartDetailNumberOfActiveSupplier(month, vendorId: number): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetChartDetailNumberOfActiveSupplier?month=" + month + "&vendorId=" + vendorId)
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetChartDetailNumberOfActiveSupplier")
      console.log(error)
    }))
  }

  GetDetailSupplierByCountry(countryCd): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetDetailSupplierByCountry?countryCd=" + countryCd )
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetDetailSupplierByCountry")
      console.log(error)
    }))
  }

  GetDetailTotalSupplierPOByCountryVendor(vendorId, page, limit): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetDetailTotalSupplierPOByCountryVendor?vendorId=" + vendorId + "&page=" + page + "&limit=" + limit )
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetDetailTotalSupplierPOByCountryVendor")
      console.log(error)
    }))
  }
  
  getSetupSales(type): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/dashboardsales?param=" + type)
    .pipe(map(res => {
      return res;
    }))
  }
  
  saveFilterSales(param: any) : Observable<any>{
    return this.httpClient.post(this.APIURL + "/api/dashboard/saveFilterSales", param)
    .pipe(map(res => {
      return res;
    }))
  }
  
}
