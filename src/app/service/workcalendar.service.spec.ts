import { TestBed } from '@angular/core/testing';

import { WorkcalendarService } from './workcalendar.service';

describe('WorkcalendarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkcalendarService = TestBed.get(WorkcalendarService);
    expect(service).toBeTruthy();
  });
});
