import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StockMonitoringModel } from '../model/stockmonitoring.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  
  
  private APIURL: string = window["SofiConfig"].GatewayApi;
  constructor(private httpClient: HttpClient) { }
 
  getUsername(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/setup/username")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("Dashboard")
      console.log(error)
    }))
  }

  getStatus(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatageneral/status")
    .pipe(map(res => {
      return res;
    }))
  }

  getWarehouse(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatadistribution/warehouse")
    .pipe(map(res => {
      return res;
    }))
  }

  getItemPart(search): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/staticdatainventory/searchpart?search=" + search)
    .pipe(map(res => {
      return res;
    }))
  }
 
  getStockMonitoring(data: StockMonitoringModel): Observable<any>{
    return this.httpClient.post(this.APIURL + "/api/dashboard/GetStock", data)
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getDivision")
      console.log(error)
    }))
  }

  getDivision(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatageneral/division")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getDivision")
      console.log(error)
    }))
  }

  getSetupProcurement(type): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/dashboardprocurement?param=" + type)
    .pipe(map(res => {
      return res;
    }))
  }

  
  getDepartment(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatageneral/department")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getDepartment")
      console.log(error)
    }))
  }
  getProject(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatageneral/project")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getProject")
      console.log(error)
    }))
  }

  getPartGroup(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatainventory/partgrouping")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getProject")
      console.log(error)
    }))
  }

  getPartCat(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatainventory/partcategory")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getProject")
      console.log(error)
    }))
  }

  getSupplier(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatadistribution/supplier")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getProject")
      console.log(error)
    }))
  }

  getSupplierBySearch(search: string) {
    return this.httpClient.get(this.APIURL + "/api/staticdatadistribution/searchsupplier?search=" + search)
    .pipe(map(res => {
      return res;
    }))
  }

  getPriority(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatageneral/priority")
    .pipe(map(res => {
      return res;
    }))
  }

  
  getRequestor(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatageneral/employee")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getRequestor")
      console.log(error)
    }))
  }

  getPOType(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/po/potype")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getPOType")
      console.log(error)
    }))
  }

  getCurrency(): Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/staticdatageneral/currency")
    .pipe(map(res => {
      return res;
    },error => {
      console.log("getCurrency")
      console.log(error)
    }))
  }
  
}
