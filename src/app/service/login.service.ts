import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestOptions,Headers, Http } from '@angular/http';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { ShowhidemenuService } from './showhidemenu.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userName: String;
  token: string;
  reload;
  decodeToken;
  headOffice;
  private authserverUrl: string = window["SofiConfig"].AuthApi;
  constructor(
    private router: Router,
    private cookieService:CookieService,
    private showService: ShowhidemenuService,
    private http: Http,
    private httpClient: HttpClient) 
    {
      this.showService.changeShowMenu(false);
    }

  getToken(usr: string, passwd: string): Observable<any> {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    const options = new RequestOptions({ headers: headers });

    const user = new HttpParams()
      .set("username", usr)
      .set("password", passwd)
      .set("grant_type", "password")
      .set("client_id", "roclient.reuse")
      .set("client_secret", "secret")
      .set("scope", "api offline_access");

    return this.http
      .post(this.authserverUrl + "/connect/token", user.toString(), options)
      .pipe(map(res => res.text()));
  }

  


  logout() {
    localStorage.clear();
    this.cookieService.deleteAll();
    this.showService.changeShowMenu(false);
    return true;
 }

 

  login(usr: string, passwd: string): Observable<any> {
    this.userName = usr;
    
    return this.getToken(usr, passwd).pipe(map(
      data => {
        // console.log(data);
        const result = JSON.parse(data);
        localStorage.setItem("token", result.access_token);
        localStorage.setItem("refresh", result.refresh_token);

        this.cookieService.set('token', result.access_token, 0, "/", ".soficloud.com");
        this.cookieService.set('refresh', result.refresh_token, 0, "/", ".soficloud.com");
        // //localhost
        // this.cookieService.set('token', result.access_token);
        // this.cookieService.set('refresh', result.refresh_token);
        
        this.router.navigate([""]);
        this.showService.changeShowMenu(true);
        // console.log(result);
        return "Ok";
      },
      error => {
        const result = JSON.parse(error._body);
        return result.Message;
      }
    ));
  }

  checkCredentials(key: string) {
    if (localStorage.getItem(key) === null) {
      this.router.navigate(["login"]);
    }
  }

  public getUserName() {
    return this.userName;
  }

  private SignOut() {
    const token = this.cookieService.get("token");
    return this.httpClient.get(this.authserverUrl + "/connect/endsession?id_token_hint="+ token )
      .pipe(map(res => res,
        error => {
          console.log("error auth logout", error)
        }))
  }
}
