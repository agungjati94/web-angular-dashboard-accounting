import { TestBed } from '@angular/core/testing';

import { ProcrumentService } from './procrument.service';

describe('ProcrumentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProcrumentService = TestBed.get(ProcrumentService);
    expect(service).toBeTruthy();
  });
});
