import { Injectable } from '@angular/core';

@Injectable()
export class SalesModel {
  countSQNeedToConfirm: number = 0;
  countSQNeedToRunning: number = 0;
  countOutstandingSQAgainstSO: number = 0;
  countOutstandingSQAgainstDO: number = 0;
  countSONeedtoConfirm: number = 0;
  countSONeedtoRunning: number = 0;
  countOutstandingSOAgainstDO: number = 0;
  countOutstandingSOAgainstInvoice: number = 0;
  numberOfSO: any;
  numberOfSQ: any;
  openSOValue: any;
  numberOfActiveCustomer: any;
  customer: any;
  salesSummary: any;
}