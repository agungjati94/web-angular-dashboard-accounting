import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
// import { JwtHelperService  } from "@auth0/angular-jwt";
// import { JwtHelper } from "angular2-jwt";
import { Http, RequestOptions, Headers, Response } from "@angular/http";
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService {
  // autUrl: string;
  // jwtHelper: JwtHelperService  = new JwtHelperService ();
  private authserverUrl: string = window["SofiConfig"].AuthApi;
  constructor(private http: Http) {}

  public getToken(): string {
    return localStorage.getItem("token");
  }

  public getRefreshToken(): string {
    return localStorage.getItem("refresh");
  }

  public getNewAcessToken() {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    const options = new RequestOptions({ headers: headers });

    const user = new HttpParams()
      .set("refresh_token", this.getRefreshToken())
      .set("grant_type", "refresh_token")
      .set("client_id", "roclient.reuse")
      .set("client_secret", "secret")
      .set("scope", "api offline_access");

    return this.http
      .post(this.authserverUrl + "/connect/token", user.toString(), options)
      .pipe(map(res => { return res}));
  }
}
