import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SalesService {
  private APIURL : string = window["SofiConfig"].GatewayApi;
  constructor(private httpClient: HttpClient) { }

  CountSQNeedtoConfirm() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSQNeedtoConfirm")
    .pipe(map(res => {
      return res;
    }))
  }

  CountSQNeedtoRunning() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSQNeedtoRunning")
    .pipe(map(res => {
      return res;
    }))
  }
  CountSQOutstandingAgainstSO() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSQOutstandingAgainstSO")
    .pipe(map(res => {
      return res;
    }))
  }

  GetSalesDetail(type) : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetSalesDetail?type=" + type)
    .pipe(map(res => {
      return res;
    }))
  }

  CountSONeedtoConfirm() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSONeedtoConfirm")
    .pipe(map(res => {
      return res;
    }))
  }

  CountSONeedtoRunning() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSONeedtoRunning")
    .pipe(map(res => {
      return res;
    }))
  }

  CountSOOutstandingAgainstDO() : Observable<any>{
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSOOutstandingAgainstDO")
    .pipe(map(res => {
      return res;
    }))
  }
  
  

  CountSOOutstandingAgainstInvoice(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSOOutstandingAgainstInvoice")
    .pipe(map(res => {
      return res;
    }))
  }

  CountSQOutstandingAgainstDO(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/CountSQOutstandingAgainstDO")
    .pipe(map(res => {
      return res;
    }))
  }

  GetChartSalesDetail(type, month): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetChartSalesDetail?type=" + type + "&month=" + month)
    .pipe(map(res => {
      return res;
    }))
  }

  GetSONumber(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/SONumber")
    .pipe(map(res => {
      return res;
    }))
  }

  GetSQNumber(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/SQNumber")
    .pipe(map(res => {
      return res;
    }))
  }

  GetOpenSOValue(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/OpenSOValue")
    .pipe(map(res => {
      return res;
    }))
  }

  GetNumberOfCustomer(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/NumberOfCustomer")
    .pipe(map(res => {
      return res;
    }))
  }

  GetTotalCustomer(): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/TotalCustomer")
    .pipe(map(res => {
      return res;
    }))
  }

  
  GetChartDetail(type, month): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetChartSalesDetail?type=" + type + "&month=" + month )
    .pipe(map(res => {
      return res;
    }))
  }

  GetChartDetailOpenSOValue(type, month, currency): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetChartDetailOpenSOValue?type=" + type + "&month=" + month + "&currencyId=" + currency)
    .pipe(map(res => {
      return res;
    }))
  }

  GetChartDetailNumberOfActiveCustomer(month: number): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetChartDetailNumberOfActiveCustomer?month=" + month)
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetChartDetailNumberOfActiveSupplier")
      console.log(error)
    }))
  }

  GetDetailCustomerByCountry(countryCd): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetDetailCustomerByCountry?countryCd=" + countryCd )
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetDetailSupplierByCountry")
      console.log(error)
    }))
  }

  GetDetailTransactionByVendor(vendorId, page, limit): Observable<any> {
    return this.httpClient.get(this.APIURL + "/api/dashboard/GetDetailTotalCustomerSOByCountryVendor?vendorId=" + vendorId + "&page=" + page + "&limit=" + limit )
    .pipe(map(res => {
      return res;
    },error => {
      console.log("GetDetailTotalSupplierPOByCountryVendor")
      console.log(error)
    }))
  }

  GetSalesSummary(){
    return this.httpClient.get(this.APIURL + "/api/dashboard/SalesSummary")
    .pipe(map(res => {
      return res;
    }))
  }


  
  

}
