import {
    Injectable, Injector,
    // Injector
   } from '@angular/core';
  import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse,
    HttpResponse
  } from '@angular/common/http';
  import { Router } from '@angular/router';
  import { catchError, map } from 'rxjs/operators';
  import { Observable, throwError } from 'rxjs';
  import { AuthService } from '../service/auth.service';
  import 'rxjs/add/operator/mergeMap';
import { CookieService } from 'ngx-cookie-service';
   @Injectable()
  export class TokenInterceptor implements HttpInterceptor {
     constructor(
      private cookieService: CookieService,
      private injector: Injector, 
      private router: Router
    ) { }

    private applyCredentials = (req: HttpRequest<any>, token: string) => {
        // console.log("Token :"+token) 
        return req.clone({
          headers: req.headers.set("Authorization", "Bearer " + token)
        });
      };

     intercept(
        req: HttpRequest<any>, 
        next: HttpHandler
        ): Observable<HttpEvent<any>> {
      const auth = this.injector.get(AuthService);
      const authRequest = this.applyCredentials(req, auth.getToken());

       return next.handle(authRequest)
       .pipe(map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          return event;
        }
      }))
        .pipe(
          catchError(err => {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401 ) {
                 //debugger
                //console.log("Unauthorized");
                return auth.getNewAcessToken().flatMap(res => {
                  //console.log(res);
                  const token = JSON.parse(res["_body"]);
                  localStorage.setItem("token", token.access_token);
                  this.cookieService.set('token', token.access_token, 0, "/", ".soficloud.com");
                this.cookieService.set('refresh', token.refresh_token, 0, "/", ".soficloud.com");

                //localhost
                // this.cookieService.set('token', token.access_token);
                // this.cookieService.set('refresh', token.refresh_token);

                  localStorage.setItem("refresh", token.refresh_token);
                  return next.handle(this.applyCredentials(req, auth.getToken()));
                });
              } else if (err.status === 403 || localStorage.getItem("token") === "undefined") {
                localStorage.clear();
                this.router.navigate(["login"]);
              }else {
                return throwError(err);
              }
            } else {
              return throwError(err);
            }
          })
        );
    }
  }