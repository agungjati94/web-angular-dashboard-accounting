export default class ProcurementModel{
    mprConfirm : number = 0;
    mprRunning : number = 0 ;
    mprOsPO : number = 0;
    mprOsGRN : number = 0;

    poConfirm : number = 0;
    poRunning : number = 0;
    poOsGRN : number = 0;
    poOsSI : number = 0;
}