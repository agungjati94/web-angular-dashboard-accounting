export enum Status{
    Entry = 'Entry',
    Confirm = 'Confirm',
    Running = 'Running',
    Revise= 'Revise',
    Pending = 'Pending',
    Cancel = 'Cancel'
}