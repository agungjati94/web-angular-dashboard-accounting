export class StockMonitoringModel {
    public PartCD: string;
    public GroupingCD: string;
    public PartCat1CD:string;
    public PartCat2CD: string;
    public PartCat3CD: string;
    public VendorCD: string;
    public DepartmentCD: string;
    public DivisionCD: string;
    public ProjectCD: string;
    public WarehouseCD: string;
    public Priority: string;
    public TransDate? : Date = new Date();
    public Page: number;
    public Limit: number;
    public TotalRow: number;
}