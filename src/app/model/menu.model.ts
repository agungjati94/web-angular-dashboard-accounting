export default class MenuModel {
    constructor( mName : string,mLink : string, mIcon : string)
    {
        this.link = mLink
        this.name = mName
        this.icon = mIcon
    }

    link: string;
    name: string;
    icon: string;
}