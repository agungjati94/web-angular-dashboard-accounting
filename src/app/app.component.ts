import { Component } from '@angular/core';
import { LoginService } from './service/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SofiCloudWeb';
  userLogin:boolean;

  constructor(private router:Router){}

  ngOnInit() {
    if (localStorage.getItem("token") != null) {
      this.userLogin = true;
    }else{
      this.userLogin = false;
      this.router.navigate([""]);
    }
    // console.log(this.userLogin)
  }

  login(value){
    this.userLogin = value;
  }

  logout(value){
    this.userLogin = value;
  }
}
