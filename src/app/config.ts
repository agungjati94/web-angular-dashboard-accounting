class Config {
  public static getHomeUrl() {
    return '/';
  }
  
  public static getMPRUrl() {
    return '/mpr/#';
    //  return 'http://localhost:4200/';
  }

  public static getPOUrl() {
    return '/po/#';
    //  return 'http://localhost:4200/';
  }

  public static getGRNUrl() {
    return '/grn/#';
    //  return 'http://localhost:4200/';
  }

  public static getSIUrl() {
    return '/si/#';
    //  return 'http://localhost:4200/';
  }

  public static getPaymentUrl() {
    return '/pay/#';
    //  return 'http://localhost:4200/';
  }

  public static getLoginUrl() {
    return '/user/#/login';
    //  return 'http://localhost:4200/';
  }

}

export default Config