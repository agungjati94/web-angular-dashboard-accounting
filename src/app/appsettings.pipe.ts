import { Pipe, PipeTransform } from '@angular/core';
import Config from './config'

@Pipe({
  name: 'appsettings'
})
export class AppsettingsPipe implements PipeTransform {
  
  transform(
    //pancingan
    output: string,
    input: string
  )
    : any {
      if (input == "home")
      return Config.getHomeUrl()
    if (input == "mpr")
      return Config.getMPRUrl()
    if (input == "grn")
      return Config.getGRNUrl()
    if (input == "po")
      return Config.getPOUrl()
    if (input == "si")
      return Config.getSIUrl()
    if (input == "payment")
      return Config.getPaymentUrl()
    if (input == "logout")
      return Config.getLoginUrl()
  }

}
