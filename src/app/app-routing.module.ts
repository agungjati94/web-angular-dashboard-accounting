import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcurementComponent } from './pages/procurement/procurement.component';
import { WorkcalendarComponent } from './pages/workcalendar/workcalendar.component';
import { StockmonitoringComponent } from './pages/stockmonitoring/stockmonitoring.component';

const routes: Routes = [
  { path: '', component: ProcurementComponent, pathMatch: 'full' },
  { path: 'Calendar', component: WorkcalendarComponent, pathMatch: 'full' },
  { path: 'StockMonitoring', component: StockmonitoringComponent, pathMatch: 'full' },
  {
    path: 'Sales',
    loadChildren: `./pages/sales/sales.module#SalesModule`
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
