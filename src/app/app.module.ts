import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from "@angular/flex-layout";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPrintModule } from 'ngx-print';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ApplicationsComponent } from './theme/applications/applications.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MAT_DATE_LOCALE } from '@angular/material';
import { LoginComponent } from './pages/login/login.component';
import { TokenInterceptor } from './interceptor/token-interceptor';
import { HttpModule } from '@angular/http';

import { CookieService } from 'ngx-cookie-service';
import { AppsettingsPipe } from './appsettings.pipe';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { LoginService } from './service/login.service';
import { AuthService } from './service/auth.service';
import { DashboardService } from './service/dashboard.service';
import { ProcurementComponent } from './pages/procurement/procurement.component';
import { FilterComponent } from './shared/filter/filter.component';
import { ProcrumentService } from './service/procrument.service';
import { CardprocurementComponent } from './pages/procurement/cardprocurement/cardprocurement.component';
import { ProcurementdetailComponent } from './shared/procurementdetail/procurementdetail.component';
import { ChartponumberComponent } from './pages/procurement/chartponumber/chartponumber.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartopenpovalueComponent } from './pages/procurement/chartopenpovalue/chartopenpovalue.component';
import { ChartnumberofsuppliersComponent } from './pages/procurement/chartnumberofsuppliers/chartnumberofsuppliers.component';
import { CharttotalsupplierComponent } from './pages/procurement/charttotalsupplier/charttotalsupplier.component';
import { ProcurementchartdetailComponent } from './shared/procurementchartdetail/procurementchartdetail.component';
import { WorkcalendarComponent } from './pages/workcalendar/workcalendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { WorkcalendarService } from './service/workcalendar.service';
import { StockmonitoringComponent } from './pages/stockmonitoring/stockmonitoring.component';
import Config from './config';
import { ProcurementModel } from './service/procurementmodel.service';
import { ChartmprnumberComponent } from './pages/procurement/chartmprnumber/chartmprnumber.component';
import { SalesModel } from './service/salesmodel.service';
import { FilterSalesComponent } from './shared/filter-sales/filter-sales.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  decimal: ",",
  precision: 2,
  prefix: "",
  suffix: "",
  thousands: "."
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    ApplicationsComponent,
    AppsettingsPipe,
    LoginComponent,
    ProcurementComponent,
    FilterComponent,
    CardprocurementComponent,
    ProcurementdetailComponent,
    ChartponumberComponent,
    ChartopenpovalueComponent,
    ChartnumberofsuppliersComponent,
    CharttotalsupplierComponent,
    ProcurementchartdetailComponent,
    WorkcalendarComponent,
    StockmonitoringComponent,
    ChartmprnumberComponent,
    FilterSalesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    DragDropModule,
    NgxPrintModule,
    InfiniteScrollModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    CurrencyMaskModule,
    NgxChartsModule,
  ],

  providers: [
    ProcurementModel,
    SalesModel,
    LoginService,
    AuthService,
    DashboardService,
    CookieService,
    ProcrumentService,
    WorkcalendarService,
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
    { provide: 'MPRURL', useFactory: Config.getMPRUrl },
    { provide: 'POURL', useFactory: Config.getPOUrl },
    { provide: 'LOGIN_URL', useFactory: Config.getLoginUrl },
    { provide: MAT_DATE_LOCALE, useValue: 'en-US' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ],
  entryComponents: [
    FilterComponent,
    ProcurementdetailComponent,
    ProcurementchartdetailComponent,
    FilterSalesComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

