import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/service/login.service';
import { ShowhidemenuService } from 'src/app/service/showhidemenu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form : FormGroup;
  username: string;
  password: string;
  @Output() userLogin : EventEmitter<any> = new EventEmitter<any>();
  errorMsg: string;

  constructor(private router:Router, private loginService:LoginService, private formBuilders:FormBuilder, private showMenu: ShowhidemenuService, private snackBar:MatSnackBar) { 
    this.form = this.formBuilders.group({
      'username': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])]
    })
  }

  ngOnInit() {
  }

  public onSubmit():void{
    let username = this.form.value.username
    let password = this.form.value.password
    if(this.form.valid){
      this.loginService.login(username, password).subscribe(res => {
        if(res !== "Ok"){
          this.errorMsg = res
        }else{
          this.userLogin.emit(true);
          this.router.navigate([""]);
          this.showMenu.changeShowMenu(true);
        }
      },
      error => {
        const messg = JSON.parse(error._body);
        this.errorMsg = messg.Message;
      })
    }
  }
}
