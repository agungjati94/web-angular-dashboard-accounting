import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  isSameDay,
  isSameMonth,
} from 'date-fns';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import { WorkcalendarService } from 'src/app/service/workcalendar.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-workcalendar',
  templateUrl: './workcalendar.component.html',
  styleUrls: ['./workcalendar.component.css']
})
export class WorkcalendarComponent implements OnInit {
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  events: CalendarEvent[];
  activeDayIsOpen: boolean = false;
  dueMPR: boolean = false
  duePO: boolean = false
  isLoading: boolean = false;
  constructor(private workCalendarservice: WorkcalendarService, 
    private snackBar: MatSnackBar, 
    @Inject("MPRURL") private MPRURL: string,
    @Inject("POURL") private POURL: string
    ) { }

  ngOnInit() {
    const today = new Date().toLocaleString()
    this.GetCalendar(today);
  }

  onGoToday() {
    const today = new Date().toLocaleString()
    this.GetCalendar(today);
  }
  private GetCalendar(today: string) {
    let types: string[] = [];
    if (this.duePO)
    types.push("DuePO")
    if (this.dueMPR)
    types.push("DueMPR")
    
    if (types.length) {
      this.isLoading = true
      this.workCalendarservice.GetDue(types, today).subscribe((calendarRes: CalendarEvent[]) => {
        this.isLoading = false
        this.events = calendarRes.map(calendar => {
          calendar.start = new Date(calendar.start)
          calendar.end = new Date(calendar.end)
          return calendar
        })
      }, error => {
        this.displayError(error);
      });
    } else {
      this.events = new Array<CalendarEvent>()
    }
  }

  private displayError(error: any) {
    this.isLoading = false
    this.snackBar.open(error.error.Message || "Sorry, an error ocurred", "X", {
      verticalPosition: 'top'
    });
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    console.log("masuk")
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }

  }

  handleEvent(action: string, event: CalendarEvent): void {
    if (event["type"] == "PO") {
      window.open(this.POURL + "/Monitoring/"  + event.id, '_blank')
    }
    else {
      window.open(this.MPRURL + "/Monitoring/" + event.id, '_blank')
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {

    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
  }

  closeOpenMonthViewDay() {
    this.GetCalendar(this.viewDate.toLocaleString())
    this.activeDayIsOpen = false;
  }

  onFilter() {
    this.GetCalendar(this.viewDate.toLocaleString())
  }

}
