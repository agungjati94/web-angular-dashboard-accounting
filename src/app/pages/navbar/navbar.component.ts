import { Component, OnInit, Output, EventEmitter, Inject} from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { DashboardService } from 'src/app/service/dashboard.service';
import { LoginService } from 'src/app/service/login.service';
import Config from 'src/app/config';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output() userLogin: EventEmitter<any> = new EventEmitter<any>();
  username:string ;
  link: string;
  constructor(private dashboardService:DashboardService, private snackbar: MatSnackBar, private loginService: LoginService, @Inject("LOGIN_URL") private LoginUrl: string) { }
  
  ngOnInit() {
    this.dashboardService.getUsername().subscribe((username) => {
      this.username = username;
    },
    error => {
      console.log(error)
      this.snackbar.open("Cannot Connect To Database Server", "X", {
        duration:5000,
        verticalPosition:'top'
      })
      this.userLogin.emit(false)
    });
  }

  logout(){
    this.loginService.logout()
    window.open(Config.getLoginUrl(), '_self')
  }
}


