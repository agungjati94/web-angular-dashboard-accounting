import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RouterOutlet, Router } from '@angular/router';
import { slideInAnimation } from '../animations';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from 'src/app/service/login.service';
import MenuModel from 'src/app/model/menu.model';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
    slideInAnimation
  ]
})
export class DashboardComponent implements OnInit {
  screenWidth: number;
  @Output() userLogin: EventEmitter<any> = new EventEmitter<any>();
  public menu : Array<MenuModel> = []

  constructor(private loginService: LoginService,
    @Inject("MPRURL") private MPRURL: string,
    public translate: TranslateService,
    private dialog: MatDialog,
    private route : Router) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
      translate.setDefaultLang('English')

    };
  }

  ngOnInit() {
    this.translate.setDefaultLang('English')
    this.menu =  [ 
      new MenuModel("Procurement", "/", "./assets/icons8-procurement-20.png"), 
      new MenuModel("Sales", "/Sales", "./assets/icons8-procurement-20.png"), 
      new MenuModel("Work Calendar", "/Calendar", "./assets/icons8-calendar-20.png"),
      new MenuModel("Material Requirement Planning", "/StockMonitoring", "./assets/icons8-calendar-20.png"),
      new MenuModel("Document Monitoring", this.MPRURL + "/Monitoring", "./assets/icons8-calendar-20.png"),
    ]
  }

  userLogout(value) {
    this.loginService.logout()
    this.userLogin.emit(value)
  }

  gotToRoute(menu : MenuModel){
    if(menu.name == "Document Monitoring"){
      window.open(menu.link, "_blank")
    }else{
      this.route.navigate([menu.link])
    }
    
  }

}