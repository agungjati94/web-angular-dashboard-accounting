import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartOpenSoValueComponent } from './chart-open-so-value.component';

describe('ChartOpenSoValueComponent', () => {
  let component: ChartOpenSoValueComponent;
  let fixture: ComponentFixture<ChartOpenSoValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartOpenSoValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartOpenSoValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
