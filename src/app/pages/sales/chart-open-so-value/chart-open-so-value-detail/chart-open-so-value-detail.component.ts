import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { SalesService } from 'src/app/service/sales.service';
import { ChartTotalCustomerTrxDetailComponent } from '../../chart-total-customer/chart-total-customer-trx-detail/chart-total-customer-trx-detail.component';

@Component({
  selector: 'app-chart-open-so-value-detail',
  templateUrl: './chart-open-so-value-detail.component.html',
  styleUrls: ['./chart-open-so-value-detail.component.css']
})
export class ChartOpenSoValueDetailComponent implements OnInit {
  
  displayedColumns: string[] = ['no', 'cd', 'date', 'supplier', 'amount'];
  dataSource : MatTableDataSource<any>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
   @Inject("MPRURL") private MPRURL : string,
   @Inject("POURL") private POURL : string,
   private service: SalesService,
   private snackBar : MatSnackBar,
  public dialogRef: MatDialogRef<ChartTotalCustomerTrxDetailComponent>) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.data.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  
 onGoToDetailTrx(id){
      window.open("/soT/"+ id, '_blank');
  }

}


