import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartOpenSoValueDetailComponent } from './chart-open-so-value-detail.component';

describe('ChartOpenSoValueDetailComponent', () => {
  let component: ChartOpenSoValueDetailComponent;
  let fixture: ComponentFixture<ChartOpenSoValueDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartOpenSoValueDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartOpenSoValueDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
