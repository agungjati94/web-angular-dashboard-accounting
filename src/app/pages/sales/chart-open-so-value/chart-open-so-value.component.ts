import { Component, OnInit, Input } from '@angular/core';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FilterSalesComponent } from 'src/app/shared/filter-sales/filter-sales.component';
import { ChartOpenSoValueDetailComponent } from './chart-open-so-value-detail/chart-open-so-value-detail.component';

@Component({
  selector: 'app-chart-open-so-value',
  templateUrl: './chart-open-so-value.component.html',
  styleUrls: ['./chart-open-so-value.component.css']
})
export class ChartOpenSoValueComponent implements OnInit {
  single: any[];
  opensovaluedata: any;
  @Input() type: string;
  @Input() chartModel: SalesModel
  IsError: boolean;
  screenWidth: number
  IsFiltered: boolean;
  IsLoading: boolean;
  constructor(private service : SalesService, private dialog : MatDialog, private snackBar : MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if(this.screenWidth < 600){
      this.view = [350, 400];
    }
    this.onGetOpenSOValue()
  }
  view: any[] = [700, 400];

  
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Open SO Value';

  colorScheme = {
    domain: ['#e6ee9c', '#ffe082', '#d32f2f', '#64b5f6', '#4db6ac', '#66bb6a']
  };

  onRefresh(){
    this.IsLoading = true;
    this.opensovaluedata = this.chartModel.openSOValue

    this.onLoadResetProp()
    this.service.GetOpenSOValue().subscribe(({ data, isFiltered}) => {
      this.opensovaluedata = data
      this.IsFiltered = isFiltered
      this.chartModel.openSOValue = data
    this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    console.log(chart)
    this.service.GetChartDetailOpenSOValue(this.type, chart.series, chart.currencyId).subscribe((data: any) => {
      this.dialog.open(ChartOpenSoValueDetailComponent, {
        width: '1000px',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter(){
    let dialog = this.dialog.open(FilterSalesComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetOpenSOValue()
    })
  }

  private onLoadResetProp() : void {
    if(this.chartModel.openSOValue == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }
  
  onGetOpenSOValue() {
    this.opensovaluedata = this.chartModel.openSOValue
    
  this.onLoadResetProp()
    this.service.GetOpenSOValue().subscribe(({ data, isFiltered}) => {
      this.opensovaluedata = data
      this.IsFiltered = isFiltered
      this.chartModel.openSOValue = data
    this.IsLoading = false

    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}


