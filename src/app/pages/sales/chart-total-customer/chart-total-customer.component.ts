import { Component, OnInit, Input } from '@angular/core';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { CardSalesDetailComponent } from '../card-sales/card-sales-detail/card-sales-detail.component';
import { ChartTotalCustomerDetailComponent } from './chart-total-customer-detail/chart-total-customer-detail.component';

@Component({
  selector: 'app-chart-total-customer',
  templateUrl: './chart-total-customer.component.html',
  styleUrls: ['./chart-total-customer.component.css']
})
export class ChartTotalCustomerComponent implements OnInit {
  totalcustomer: any;
  IsError: boolean;
  @Input() chartModel: SalesModel
  screenWidth: number;
  total: number;
  IsLoading: boolean;
  constructor(private service: SalesService, private snackBar: MatSnackBar, private dialog: MatDialog) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if (this.screenWidth < 600) {
      this.view = [350, 400];
    }
    this.onGetTotalCustomer()

   
  }
  view: any[] = [450, 400];

  colorScheme = {
    domain: ['#9a0007', '#4a0072', '#e64a19', '#003300', '#fbc02d', '#66bb6a']
  };

  showLabels = true;
  explodeSlices = false;
  doughnut = true;

  onSelect(event) {
    
    this.service.GetDetailCustomerByCountry(event.name).subscribe(data => {
      this.dialog.open(ChartTotalCustomerDetailComponent, {
        width: '1000px',
        height: '90vh',
        data: { data: data, type: "TotalSupplier" }
      })
    })
  }
  private onLoadResetProp() : void {
    if (this.chartModel.customer == null) this.IsLoading = true

    this.IsError = false
  }
  onGetTotalCustomer() {
    this.totalcustomer = this.chartModel.customer
    
    this.onLoadResetProp()
    this.service.GetTotalCustomer().subscribe(({ data, total }) => {
      this.totalcustomer = data
      this.total = total
      this.chartModel.customer = data
    this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}

