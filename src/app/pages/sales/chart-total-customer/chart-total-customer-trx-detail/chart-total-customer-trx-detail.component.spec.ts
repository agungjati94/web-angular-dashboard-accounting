import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartTotalCustomerTrxDetailComponent } from './chart-total-customer-trx-detail.component';

describe('ChartTotalCustomerTrxDetailComponent', () => {
  let component: ChartTotalCustomerTrxDetailComponent;
  let fixture: ComponentFixture<ChartTotalCustomerTrxDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartTotalCustomerTrxDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTotalCustomerTrxDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
