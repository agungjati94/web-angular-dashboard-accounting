import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import { SalesService } from 'src/app/service/sales.service';
import { CardSalesDetailComponent } from '../../card-sales/card-sales-detail/card-sales-detail.component';
import { ChartTotalCustomerTrxDetailComponent } from '../chart-total-customer-trx-detail/chart-total-customer-trx-detail.component';

@Component({
  selector: 'app-chart-total-customer-detail',
  templateUrl: './chart-total-customer-detail.component.html',
  styleUrls: ['./chart-total-customer-detail.component.css']
})
export class ChartTotalCustomerDetailComponent implements OnInit {
  displayedColumns: string[] = ['no', 'name', 'phone', 'pic', 'address'];
  dataSource : MatTableDataSource<any>;
  onLoading :boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
   @Inject("MPRURL") private MPRURL : string,
   @Inject("POURL") private POURL : string,
   private service: SalesService,
   private dialog: MatDialog, 
  public dialogRef: MatDialogRef<CardSalesDetailComponent>) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.data.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onGoToDetailTrx({vendorId}){
    this.service.GetDetailTransactionByVendor(vendorId, 1, 20).subscribe((data) => {
      this.dialog.open(ChartTotalCustomerTrxDetailComponent, {
        width: '1000px',
        height: '90vh',
        data: { data, vendorId, type: "TotalCustomerTransaksi" }
      })
    })
  }
  
}
