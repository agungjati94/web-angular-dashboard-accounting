import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartTotalCustomerDetailComponent } from './chart-total-customer-detail.component';

describe('ChartTotalCustomerDetailComponent', () => {
  let component: ChartTotalCustomerDetailComponent;
  let fixture: ComponentFixture<ChartTotalCustomerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartTotalCustomerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTotalCustomerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
