import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartTotalCustomerComponent } from './chart-total-customer.component';

describe('ChartTotalSupplierComponent', () => {
  let component: ChartTotalCustomerComponent;
  let fixture: ComponentFixture<ChartTotalCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartTotalCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTotalCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
