import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesComponent } from './sales.component';
import { SalesRoutingModule } from './sales-routing.module';
import { MaterialModule } from 'src/app/material/material.module';
import { CardSalesComponent } from './card-sales/card-sales.component';
import { ChartSqNumberComponent } from './chart-sq-number/chart-sq-number.component';
import { ChartSoNumberComponent } from './chart-so-number/chart-so-number.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartOpenSoValueComponent } from './chart-open-so-value/chart-open-so-value.component';
import { ChartNumberofCustomersComponent } from './chart-numberof-customers/chart-numberof-customers.component';
import { ChartTotalCustomerComponent } from './chart-total-customer/chart-total-customer.component';
import { CardSalesDetailComponent } from './card-sales/card-sales-detail/card-sales-detail.component';
import { ChartNumberofCustomersDetailComponent } from './chart-numberof-customers/chart-numberof-customers-detail/chart-numberof-customers-detail.component';
import { ChartTotalCustomerDetailComponent } from './chart-total-customer/chart-total-customer-detail/chart-total-customer-detail.component';
import { ChartTotalCustomerTrxDetailComponent } from './chart-total-customer/chart-total-customer-trx-detail/chart-total-customer-trx-detail.component';
import { ChartNumberofCustomersTrxDetailComponent } from './chart-numberof-customers/chart-numberof-customers-trx-detail/chart-numberof-customers-trx-detail.component';
import { ChartOpenSoValueDetailComponent } from './chart-open-so-value/chart-open-so-value-detail/chart-open-so-value-detail.component';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { ChartSalesSummarybyCurrencyComponent } from './chart-sales-summaryby-currency/chart-sales-summaryby-currency.component';
import { ChartsalessummarydetailComponent } from './chart-sales-summaryby-currency/chartsalessummarydetail/chartsalessummarydetail.component';

@NgModule({
  imports: [
    CommonModule,
    SalesRoutingModule,
    MaterialModule,
    NgxChartsModule
  ],
  providers: [
    SalesModel,
    SalesService,
  ],
  declarations: [
    SalesComponent, 
    CardSalesComponent, 
    ChartSqNumberComponent, 
    ChartSoNumberComponent, 
    ChartOpenSoValueComponent, 
    ChartNumberofCustomersComponent, 
    ChartTotalCustomerComponent,
    CardSalesDetailComponent,
    ChartNumberofCustomersDetailComponent,
    ChartTotalCustomerDetailComponent,
    ChartTotalCustomerTrxDetailComponent,
    ChartNumberofCustomersTrxDetailComponent,
    ChartOpenSoValueDetailComponent,
    ChartSalesSummarybyCurrencyComponent,
    ChartsalessummarydetailComponent
  ],
  entryComponents: [
    CardSalesDetailComponent,
    ChartNumberofCustomersDetailComponent,
    ChartTotalCustomerDetailComponent,
    ChartTotalCustomerTrxDetailComponent,
    ChartNumberofCustomersTrxDetailComponent,
    ChartOpenSoValueDetailComponent,
    ChartsalessummarydetailComponent
  ]
})
export class SalesModule { }