import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSalesComponent } from './card-sales.component';

describe('CardSalesComponent', () => {
  let component: CardSalesComponent;
  let fixture: ComponentFixture<CardSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
