import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { CardSalesDetailComponent } from './card-sales-detail/card-sales-detail.component';
import { FilterSalesComponent } from 'src/app/shared/filter-sales/filter-sales.component';

@Component({
  selector: 'app-card-sales',
  templateUrl: './card-sales.component.html',
  styleUrls: ['./card-sales.component.css']
})
export class CardSalesComponent implements OnInit {
  @Input() count: number;
  @Input() description: string
  @Input() type: string
  @Input() cardModel: SalesModel
  IsLoading: boolean
  IsError : boolean
  IsFiltered : boolean;
  constructor(public dialog: MatDialog, private service: SalesService, private snackBar: MatSnackBar) {
    
   }

  ngOnInit() {
    if (this.type == "SQNeedToConfirm") {
      this.count = this.cardModel.countSQNeedToConfirm;
      this.callSQNeedToConfirm();
    } else if (this.type == "SQNeedToRunning") {
      this.count = this.cardModel.countSQNeedToRunning;
      this.callSQNeedToRunning();
    }
    else if (this.type == "OutstandingSQAgainstSO") {
      this.count = this.cardModel.countOutstandingSQAgainstSO;
      this.callOutstandingSQAgainstSO();
    }
    else if (this.type == "OutstandingSQAgainstDO") {
      this.count = this.cardModel.countOutstandingSQAgainstDO;
      this.callOutstandingSQAgainstDO();
    }
    else if (this.type == "SONeedtoConfirm") {
      this.count = this.cardModel.countSONeedtoConfirm;
      this.callSONeedtoConfirm();
    }
    else if (this.type == "SONeedtoRunning") {
      this.count = this.cardModel.countSONeedtoRunning;
      this.callSONeedtoRunning()
    }
    else if (this.type == "OutstandingSOAgainstDO") {
      this.count = this.cardModel.countOutstandingSOAgainstDO;
      this.callOutstandingSOAgainstDO();
    }
    else if (this.type == "OutstandingSOAgainstInvoice") {
      this.count = this.cardModel.countOutstandingSOAgainstInvoice;
      this.callOutstandingSOAgainstInvoice();
    }
  }

private onLoadResetProp() : void {
  this.IsError = false
  this.IsFiltered = false
}
  private callOutstandingSOAgainstInvoice() {
    if(this.cardModel.countOutstandingSOAgainstInvoice == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSOOutstandingAgainstInvoice().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingSOAgainstInvoice = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callOutstandingSOAgainstDO() {
    if(this.cardModel.countOutstandingSOAgainstDO == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSOOutstandingAgainstDO().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingSOAgainstDO = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callSONeedtoRunning() {
    if(this.cardModel.countSONeedtoRunning == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSONeedtoRunning().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countSONeedtoRunning = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callSONeedtoConfirm() {
    if(this.cardModel.countSONeedtoConfirm == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSONeedtoConfirm().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countSONeedtoConfirm = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callOutstandingSQAgainstDO() {
    if(this.cardModel.countOutstandingSQAgainstDO == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSQOutstandingAgainstDO().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingSQAgainstDO = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callOutstandingSQAgainstSO() {
    if(this.cardModel.countOutstandingSQAgainstSO == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSQOutstandingAgainstSO().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingSQAgainstSO = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callSQNeedToRunning() {
    if(this.cardModel.countSQNeedToRunning == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSQNeedtoRunning().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countSQNeedToRunning = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callSQNeedToConfirm() {
    if(this.cardModel.countSQNeedToConfirm == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.service.CountSQNeedtoConfirm().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countSQNeedToConfirm = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  onRefresh() : void {
    this.IsLoading = true
    if (this.type == "SQNeedToConfirm") {
      this.callSQNeedToConfirm();
    } else if (this.type == "SQNeedToRunning") {
      this.callSQNeedToRunning();
    }
    else if (this.type == "OutstandingSQAgainstSO") {
      this.callOutstandingSQAgainstSO();
    }
    else if (this.type == "OutstandingSQAgainstDO") {
      this.callOutstandingSQAgainstDO();
    }
    else if (this.type == "SONeedtoConfirm") {
      this.callSONeedtoConfirm();
    }
    else if (this.type == "SONeedtoRunning") {
      this.callSONeedtoRunning()
    }
    else if (this.type == "OutstandingSOAgainstDO") {
      this.callOutstandingSOAgainstDO();
    }
    else if (this.type == "OutstandingSOAgainstInvoice") {
      this.callOutstandingSOAgainstInvoice();
    }
  }

  openFormFilter(): void {
    let dialog = this.dialog.open(FilterSalesComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(refresh => {
      if(refresh) this.onRefresh()
    })
  }



  openTable(): void {
    this.service.GetSalesDetail(this.type).subscribe((data: any) => {
      this.dialog.open(CardSalesDetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  private displayError(error: any) {
    this.IsLoading = false
    this.IsError = true
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
