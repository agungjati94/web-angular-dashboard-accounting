import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSalesDetailComponent } from './card-sales-detail.component';

describe('CardSalesDetailComponent', () => {
  let component: CardSalesDetailComponent;
  let fixture: ComponentFixture<CardSalesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSalesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSalesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
