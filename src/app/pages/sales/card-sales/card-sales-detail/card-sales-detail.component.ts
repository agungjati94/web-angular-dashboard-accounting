import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { SalesService } from 'src/app/service/sales.service';

@Component({
  selector: 'app-card-sales-detail',
  templateUrl: './card-sales-detail.component.html',
  styleUrls: ['./card-sales-detail.component.css']
})
export class CardSalesDetailComponent implements OnInit {
  displayedColumns: string[] = ['no', 'cd', 'requestor', 'date', 'division', 'department', 'project', 'status'];
  dataSource : MatTableDataSource<any>;
  IsSO : boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  Limit: number = 20;
  TotalRow: any;
  onLoading: boolean;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<CardSalesDetailComponent>) { }

  ngOnInit() {
    this.IsSO = ((this.data.type == "SONeedtoConfirm") || (this.data.type == "SONeedtoRunning") ||
    (this.data.type == "OutstandingSOAgainstDO") || (this.data.type == "OutstandingSOAgainstInvoice") ||
    (this.data.type == "NumberofSO") || (this.data.type == "NumberofCustomer") || (this.data.type == "TotalCustomer"));
    this.dataSource = new MatTableDataSource<any>(this.data.data)
    this.TotalRow = (this.data.header && this.data.header.total) || 0
    this.dataSource.paginator = this.paginator;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onGoToDetailTrx(id){
    if(this.IsSO){
      window.open("/soT/"+ id, '_blank');
    }else {
      window.open("/sqT/"+ id, '_blank');
    }
  }

  

  

}
