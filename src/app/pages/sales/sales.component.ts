import { Component, OnInit } from '@angular/core';
import { SalesModel } from 'src/app/service/salesmodel.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {

  public isOpenPanelChart1 :boolean = false;
  public isOpenPanelChart2 :boolean = false;
  public isOpenPanelChart3 :boolean = false;

  constructor(public cardModel: SalesModel) { }

  ngOnInit() {
  }

}
