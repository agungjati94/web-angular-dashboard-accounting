import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartSalesSummarybyCurrencyComponent } from './chart-sales-summaryby-currency.component';

describe('ChartSalesSummarybyCurrencyComponent', () => {
  let component: ChartSalesSummarybyCurrencyComponent;
  let fixture: ComponentFixture<ChartSalesSummarybyCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartSalesSummarybyCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartSalesSummarybyCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
