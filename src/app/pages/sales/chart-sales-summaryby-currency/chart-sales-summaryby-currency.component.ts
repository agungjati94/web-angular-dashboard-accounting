import { Component, OnInit, Input } from '@angular/core';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CardSalesDetailComponent } from '../card-sales/card-sales-detail/card-sales-detail.component';
import { FilterSalesComponent } from 'src/app/shared/filter-sales/filter-sales.component';
import { ChartsalessummarydetailComponent } from './chartsalessummarydetail/chartsalessummarydetail.component';

@Component({
  selector: 'app-chart-sales-summaryby-currency',
  templateUrl: './chart-sales-summaryby-currency.component.html',
  styleUrls: ['./chart-sales-summaryby-currency.component.css']
})
export class ChartSalesSummarybyCurrencyComponent implements OnInit {
  salesSummaryData: any

  type: string = "SalesSummary"
  @Input() chartModel: SalesModel
  IsError: boolean;
  screenWidth: number;
  IsFiltered: boolean;
  IsLoading: boolean;

  constructor(private service: SalesService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if (this.screenWidth < 600) {
      this.view = [350, 400];
    }
    this.getSalesSummary()

    console.log("a", this.chartModel.salesSummary)
  }
  view: any[] = [ 1000, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Total Sales Summary';

  colorScheme = {
    domain: ['#66bb6a', '#e64a19', '#003300']
    //'#fbc02d', '#4a0072', '#9a0007', '#66bb6a', '#e64a19', '#003300', '#fbc02d', '#4a0072', '#9a0007']
  };

  onRefresh() {
    this.IsLoading = true;
    this.salesSummaryData = this.chartModel.salesSummary

    this.onLoadResetProp()
    this.service.GetSalesSummary().subscribe((res :any) => {
      this.salesSummaryData = [res]
      this.IsFiltered = res.isFiltered
      this.chartModel.salesSummary = this.salesSummaryData
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    this.IsLoading = true;
    this.service.GetChartSalesDetail(this.type, chart.name).subscribe((data: any) => {
      this.IsLoading = false;
      this.dialog.open(ChartsalessummarydetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter() {
    let dialog = this.dialog.open(FilterSalesComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.getSalesSummary()
    })
  }

  private onLoadResetProp(): void {
    if (this.chartModel.salesSummary == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }

  getSalesSummary() {
    this.salesSummaryData = this.chartModel.salesSummary

    this.onLoadResetProp()
    this.service.GetSalesSummary().subscribe((res : any) => {
      this.salesSummaryData = [res]
      this.IsFiltered = res.isFiltered
      this.chartModel.salesSummary = this.salesSummaryData
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
