import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsalessummarydetailComponent } from './chartsalessummarydetail.component';

describe('ChartsalessummarydetailComponent', () => {
  let component: ChartsalessummarydetailComponent;
  let fixture: ComponentFixture<ChartsalessummarydetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsalessummarydetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsalessummarydetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
