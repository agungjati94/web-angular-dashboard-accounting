import { Component, OnInit, Inject } from '@angular/core';
import { MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-chartsalessummarydetail',
  templateUrl: './chartsalessummarydetail.component.html',
  styleUrls: ['./chartsalessummarydetail.component.css']
})
export class ChartsalessummarydetailComponent implements OnInit {
  displayedColumns: string[] = ['no', 'product', 'qty', 'amount'];
  dataSource : MatTableDataSource<any>;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<any>) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.data.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
