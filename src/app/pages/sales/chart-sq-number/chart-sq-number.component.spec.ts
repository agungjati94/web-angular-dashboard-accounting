import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartSqNumberComponent } from './chart-sq-number.component';

describe('ChartSqNumberComponent', () => {
  let component: ChartSqNumberComponent;
  let fixture: ComponentFixture<ChartSqNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartSqNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartSqNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
