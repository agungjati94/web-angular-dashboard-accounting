import { Component, OnInit, Input } from '@angular/core';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CardSalesDetailComponent } from '../card-sales/card-sales-detail/card-sales-detail.component';
import { FilterSalesComponent } from 'src/app/shared/filter-sales/filter-sales.component';

@Component({
  selector: 'app-chart-sq-number',
  templateUrl: './chart-sq-number.component.html',
  styleUrls: ['./chart-sq-number.component.css']
})
export class ChartSqNumberComponent implements OnInit {
  single: any[];
  sqnumberdata: any;
  @Input() type: string
  @Input() chartModel: SalesModel
  IsError: boolean;
  screenWidth: number;
  IsFiltered: boolean;
  IsLoading: boolean;

  constructor(private service: SalesService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if (this.screenWidth < 600) {
      this.view = [350, 400];
    }
    this.onGetSQNumber()
  }
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Number Of SQ';

  colorScheme = {
    domain: ['#66bb6a', '#e64a19', '#003300', '#fbc02d', '#4a0072', '#9a0007']
  };

  onRefresh() {
    this.IsLoading = true;
    this.sqnumberdata = this.chartModel.numberOfSQ

    this.onLoadResetProp()
    this.service.GetSQNumber().subscribe(({ data, isFiltered }) => {
      this.sqnumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfSQ = this.sqnumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    this.IsLoading = true;
    this.service.GetChartSalesDetail(this.type, chart.name).subscribe((data: any) => {
      this.dialog.open(CardSalesDetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
      this.IsLoading = false;
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter() {
    let dialog = this.dialog.open(FilterSalesComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetSQNumber()
    })
  }

  private onLoadResetProp(): void {
    if (this.chartModel.numberOfSQ == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }

  onGetSQNumber() {
    this.sqnumberdata = this.chartModel.numberOfSQ

    this.onLoadResetProp()
    this.service.GetSQNumber().subscribe(({ data, isFiltered }) => {
      this.sqnumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfSQ = this.sqnumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
