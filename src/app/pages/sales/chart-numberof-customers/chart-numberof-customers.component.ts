import { Component, OnInit, Input } from '@angular/core';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ChartNumberofCustomersDetailComponent } from './chart-numberof-customers-detail/chart-numberof-customers-detail.component';
import { FilterSalesComponent } from 'src/app/shared/filter-sales/filter-sales.component';

@Component({
  selector: 'app-chart-numberof-customers',
  templateUrl: './chart-numberof-customers.component.html',
  styleUrls: ['./chart-numberof-customers.component.css']
})
export class ChartNumberofCustomersComponent implements OnInit {
  single: any[];
  numberofcustomer: any;
  @Input() type: string;
  @Input() chartModel: SalesModel;
  IsError: boolean;
  screenWidth: number;
  IsFiltered: boolean;
  IsLoading: boolean;
  constructor(private service : SalesService, private dialog : MatDialog, private snackBar : MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
   }

  ngOnInit() {
    if(this.screenWidth < 600){
      this.view = [350, 400];
    }
   this.onGetNumberOfCustomer()
  }
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Number Of Active Customer';

  colorScheme = {
    domain: ['#9a0007', '#4a0072', '#e64a19', '#003300', '#fbc02d', '#66bb6a']
  };

  onRefresh() {
    this.IsLoading = true;
    this.numberofcustomer = this.chartModel.numberOfActiveCustomer

    this.onLoadResetProp()
    this.service.GetNumberOfCustomer().subscribe(({ data, isFiltered }) => {
      this.numberofcustomer = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfActiveCustomer = this.numberofcustomer
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    this.service.GetChartSalesDetail(this.type, chart.name).subscribe((data: any) => {
      this.dialog.open(ChartNumberofCustomersDetailComponent, {
        width: '1000px',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter(){
    let dialog = this.dialog.open(FilterSalesComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetNumberOfCustomer()
    })
  }
  private onLoadResetProp() : void {
    if (this.chartModel.numberOfActiveCustomer == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }
  
  onGetNumberOfCustomer() {
    this.numberofcustomer = this.chartModel.numberOfActiveCustomer

    this.onLoadResetProp()
    this.service.GetNumberOfCustomer().subscribe(({ data, isFiltered}) => {
      this.numberofcustomer = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfActiveCustomer = this.numberofcustomer
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsLoading = false
    this.IsError = true
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
