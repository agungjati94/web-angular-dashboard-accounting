import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartNumberofCustomersComponent } from './chart-numberof-customers.component';

describe('ChartNumberofCustomersComponent', () => {
  let component: ChartNumberofCustomersComponent;
  let fixture: ComponentFixture<ChartNumberofCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartNumberofCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartNumberofCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
