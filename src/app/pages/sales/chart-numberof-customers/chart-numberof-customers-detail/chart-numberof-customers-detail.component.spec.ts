import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartNumberofCustomersDetailComponent } from './chart-numberof-customers-detail.component';

describe('ChartNumberofCustomersDetailComponent', () => {
  let component: ChartNumberofCustomersDetailComponent;
  let fixture: ComponentFixture<ChartNumberofCustomersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartNumberofCustomersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartNumberofCustomersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
