import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import { SalesService } from 'src/app/service/sales.service';
import { CardSalesDetailComponent } from '../../card-sales/card-sales-detail/card-sales-detail.component';
import { ChartNumberofCustomersTrxDetailComponent } from '../chart-numberof-customers-trx-detail/chart-numberof-customers-trx-detail.component';

@Component({
  selector: 'app-chart-numberof-customers-detail',
  templateUrl: './chart-numberof-customers-detail.component.html',
  styleUrls: ['./chart-numberof-customers-detail.component.css']
})
export class ChartNumberofCustomersDetailComponent implements OnInit {
  displayedColumns: string[] = ['no', 'customer', 'amount'];
  dataSource : MatTableDataSource<any>;
  onLoading :boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
   @Inject("MPRURL") private MPRURL : string,
   @Inject("POURL") private POURL : string,
   private service: SalesService,
   private snackBar : MatSnackBar,
   public dialog: MatDialog,
  public dialogRef: MatDialogRef<CardSalesDetailComponent>) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.data.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onGoToDetailTrx(id){
    this.service.GetDetailTransactionByVendor(id, 1, 20).subscribe((data) => {
      this.dialog.open(ChartNumberofCustomersTrxDetailComponent, {
        width: '1000px',
        height: '90vh',
        data: { data, vendorId: id, type: "NumberOfActiveCustomer" }
      })
    }, this.displayError)
  }

  

  private displayError(error: any) {
    this.onLoading = false
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }

}
