import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { SalesService } from 'src/app/service/sales.service';
import { ChartTotalCustomerTrxDetailComponent } from '../../chart-total-customer/chart-total-customer-trx-detail/chart-total-customer-trx-detail.component';

@Component({
  selector: 'app-chart-numberof-customers-trx-detail',
  templateUrl: './chart-numberof-customers-trx-detail.component.html',
  styleUrls: ['./chart-numberof-customers-trx-detail.component.css']
})
export class ChartNumberofCustomersTrxDetailComponent implements OnInit {
  displayedColumns: string[] = ['no', 'cd', 'requestor', 'date', 'division', 'department', 'project', 'status'];
  dataSource : MatTableDataSource<any>;
  IsLoading: boolean
  IsError : boolean
  Limit: number = 20;
  TotalRow: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
   @Inject("MPRURL") private MPRURL : string,
   @Inject("POURL") private POURL : string,
   private service: SalesService,
   private snackBar : MatSnackBar,
  public dialogRef: MatDialogRef<ChartTotalCustomerTrxDetailComponent>) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.data.data.data)
    this.TotalRow = this.data.data.header.total
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  onMovePage(e){
    this.IsLoading = true
    this.service.GetDetailTransactionByVendor(this.data.vendorId, ++e.pageIndex, this.Limit).subscribe(({data, header } : any) => {
      this.TotalRow = header.total
      this.dataSource = new MatTableDataSource<any>(data)
      this.IsLoading = false
    }, error => {
      this.displayError(error);
    })
  }

  private displayError(error: any) {
    this.IsLoading = false
    this.IsError = true
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }

 onGoToDetailTrx(id){
      window.open("/soT/"+ id, '_blank');
  }

}

