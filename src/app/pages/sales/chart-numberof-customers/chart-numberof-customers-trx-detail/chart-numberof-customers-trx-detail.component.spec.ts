import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartNumberofCustomersTrxDetailComponent } from './chart-numberof-customers-trx-detail.component';

describe('ChartNumberofCustomersTrxDetailComponent', () => {
  let component: ChartNumberofCustomersTrxDetailComponent;
  let fixture: ComponentFixture<ChartNumberofCustomersTrxDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartNumberofCustomersTrxDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartNumberofCustomersTrxDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
