import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartSoNumberComponent } from './chart-so-number.component';

describe('ChartSoNumberComponent', () => {
  let component: ChartSoNumberComponent;
  let fixture: ComponentFixture<ChartSoNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartSoNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartSoNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
