import { Component, OnInit, Input } from '@angular/core';
import { SalesModel } from 'src/app/service/salesmodel.service';
import { SalesService } from 'src/app/service/sales.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CardSalesDetailComponent } from '../card-sales/card-sales-detail/card-sales-detail.component';
import { FilterSalesComponent } from 'src/app/shared/filter-sales/filter-sales.component';

@Component({
  selector: 'app-chart-so-number',
  templateUrl: './chart-so-number.component.html',
  styleUrls: ['./chart-so-number.component.css']
})
export class ChartSoNumberComponent implements OnInit {
  single: any[];
  sonumberdata: any;
  @Input() type: string
  @Input() chartModel: SalesModel
  IsError: boolean;
  screenWidth: number;
  IsFiltered: boolean;
  IsLoading: boolean;

  constructor(private service: SalesService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if (this.screenWidth < 600) {
      this.view = [350, 400];
    }
    this.onGetSONumber()
  }
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Number Of SO';

  colorScheme = {
    domain: ['#9a0007', '#4a0072', '#fbc02d', '#003300', '#e64a19', '#66bb6a']
  };

  onRefresh() {
    this.IsLoading = true;
    this.sonumberdata = this.chartModel.numberOfSO

    this.onLoadResetProp()
    this.service.GetSONumber().subscribe(({ data, isFiltered }) => {
      this.sonumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfSO = this.sonumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    this.IsLoading = true;
    this.service.GetChartDetail(this.type, chart.name).subscribe((data: any) => {
      this.dialog.open(CardSalesDetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
      this.IsLoading = false;
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter() {
    let dialog = this.dialog.open(FilterSalesComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetSONumber()
    })
  }

  private onLoadResetProp(): void {
    if (this.chartModel.numberOfSO == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }

  onGetSONumber() {
    this.sonumberdata = this.chartModel.numberOfSO

    this.onLoadResetProp()
    this.service.GetSONumber().subscribe(({ data, isFiltered }) => {
      this.sonumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfSO = this.sonumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}

