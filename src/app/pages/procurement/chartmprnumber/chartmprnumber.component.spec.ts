import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartmprnumberComponent } from './chartmprnumber.component';

describe('ChartmprnumberComponent', () => {
  let component: ChartmprnumberComponent;
  let fixture: ComponentFixture<ChartmprnumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartmprnumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartmprnumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
