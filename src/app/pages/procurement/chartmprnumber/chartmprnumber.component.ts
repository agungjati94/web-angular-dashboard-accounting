import { Component, OnInit, Input } from '@angular/core';
import { ProcurementModel } from 'src/app/service/procurementmodel.service';
import { ProcrumentService } from 'src/app/service/procrument.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ProcurementdetailComponent } from 'src/app/shared/procurementdetail/procurementdetail.component';
import { FilterComponent } from 'src/app/shared/filter/filter.component';

@Component({
  selector: 'app-chartmprnumber',
  templateUrl: './chartmprnumber.component.html',
  styleUrls: ['./chartmprnumber.component.css']
})
export class ChartmprnumberComponent implements OnInit {
  single: any[];
  mprnumberdata: any;
  @Input() type: string
  @Input() chartModel: ProcurementModel
  IsError: boolean;
  screenWidth: number;
  IsFiltered: boolean;
  IsLoading: boolean;

  constructor(private procrumentService: ProcrumentService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if (this.screenWidth < 600) {
      this.view = [350, 400];
    }
    this.onGetMPRNumber()
  }
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Number Of MPR';

  colorScheme = {
    domain: ['#66bb6a', '#e64a19', '#003300', '#fbc02d', '#4a0072', '#9a0007']
  };

  onRefresh() {
    this.IsLoading = true;
    this.mprnumberdata = this.chartModel.numberOfMPR

    this.onLoadResetProp()
    this.procrumentService.GetMPRNumber().subscribe(({ data, isFiltered }) => {
      this.mprnumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfMPR = this.mprnumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    this.procrumentService.GetChartDetail(this.type, chart.name).subscribe((data: any) => {
      this.dialog.open(ProcurementdetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter() {
    let dialog = this.dialog.open(FilterComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetMPRNumber()
    })
  }

  private onLoadResetProp(): void {
    if (this.chartModel.numberOfMPR == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }

  onGetMPRNumber() {
    this.mprnumberdata = this.chartModel.numberOfMPR

    this.onLoadResetProp()
    this.procrumentService.GetMPRNumber().subscribe(({ data, isFiltered }) => {
      this.mprnumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfMPR = this.mprnumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
