import { Component, OnInit, Input } from '@angular/core';
import { ProcrumentService } from 'src/app/service/procrument.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ProcurementchartdetailComponent } from 'src/app/shared/procurementchartdetail/procurementchartdetail.component';
import { ProcurementModel } from 'src/app/service/procurementmodel.service';

@Component({
  selector: 'app-charttotalsupplier',
  templateUrl: './charttotalsupplier.component.html',
  styleUrls: ['./charttotalsupplier.component.css']
})
export class CharttotalsupplierComponent implements OnInit {
  totalsupplier: any;
  IsError: boolean;
  @Input() chartModel: ProcurementModel
  screenWidth: number;
  total: number;
  IsLoading: boolean;
  constructor(private procrumentService: ProcrumentService, private snackBar: MatSnackBar, private dialog: MatDialog) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if (this.screenWidth < 600) {
      this.view = [350, 400];
    }
    this.onGetTotalSupplier()
  }
  view: any[] = [450, 400];

  colorScheme = {
    domain: ['#9a0007', '#4a0072', '#e64a19', '#003300', '#fbc02d', '#66bb6a']
  };

  showLabels = true;
  explodeSlices = false;
  doughnut = true;

  onSelect(event) {
    this.procrumentService.GetDetailSupplierByCountry(event.name).subscribe(data => {
      this.dialog.open(ProcurementchartdetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: "TotalSupplier" }
      })
    })
  }
  private onLoadResetProp() : void {
    if (this.chartModel.supplier == null) this.IsLoading = true

    this.IsError = false
  }
  onGetTotalSupplier() {
    this.totalsupplier = this.chartModel.supplier
    
    this.onLoadResetProp()
    this.procrumentService.GetTotalSupplier().subscribe(({ data, total }) => {
      this.totalsupplier = data
      this.total = total
      this.chartModel.supplier = data
    this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
