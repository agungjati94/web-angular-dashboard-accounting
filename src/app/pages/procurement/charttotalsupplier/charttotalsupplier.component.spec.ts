import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharttotalsupplierComponent } from './charttotalsupplier.component';

describe('CharttotalsupplierComponent', () => {
  let component: CharttotalsupplierComponent;
  let fixture: ComponentFixture<CharttotalsupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharttotalsupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharttotalsupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
