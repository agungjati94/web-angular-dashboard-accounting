import { Component, OnInit, Input } from '@angular/core';
import { ProcrumentService } from 'src/app/service/procrument.service';
import { FilterComponent } from 'src/app/shared/filter/filter.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ProcurementdetailComponent } from 'src/app/shared/procurementdetail/procurementdetail.component';
import { ProcurementModel } from 'src/app/service/procurementmodel.service';

@Component({
  selector: 'app-chartponumber',
  templateUrl: './chartponumber.component.html',
  styleUrls: ['./chartponumber.component.css']
})
export class ChartponumberComponent implements OnInit {
  single: any[];
  ponumberdata: any;
  @Input() type: string
  @Input() chartModel: ProcurementModel
  IsError: boolean;
  screenWidth: number;
  IsFiltered: boolean;
  IsLoading: boolean;

  constructor(private procrumentService: ProcrumentService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if (this.screenWidth < 600) {
      this.view = [350, 400];
    }
    this.onGetPONumber()
  }
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Number Of PO';

  colorScheme = {
    domain: ['#9a0007', '#4a0072', '#fbc02d', '#003300', '#e64a19', '#66bb6a']
  };

  onRefresh() {
    this.IsLoading = true;
    this.ponumberdata = this.chartModel.numberOfPO

    this.onLoadResetProp()
    this.procrumentService.GetPONumber().subscribe(({ data, isFiltered }) => {
      this.ponumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfPO = this.ponumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    this.procrumentService.GetChartDetail(this.type, chart.name).subscribe((data: any) => {
      this.dialog.open(ProcurementdetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter() {
    let dialog = this.dialog.open(FilterComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetPONumber()
    })
  }

  private onLoadResetProp(): void {
    if (this.chartModel.numberOfPO == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }

  onGetPONumber() {
    this.ponumberdata = this.chartModel.numberOfPO

    this.onLoadResetProp()
    this.procrumentService.GetPONumber().subscribe(({ data, isFiltered }) => {
      this.ponumberdata = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfPO = this.ponumberdata
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
