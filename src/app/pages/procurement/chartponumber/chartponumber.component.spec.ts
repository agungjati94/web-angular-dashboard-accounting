import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartponumberComponent } from './chartponumber.component';

describe('ChartponumberComponent', () => {
  let component: ChartponumberComponent;
  let fixture: ComponentFixture<ChartponumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartponumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartponumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
