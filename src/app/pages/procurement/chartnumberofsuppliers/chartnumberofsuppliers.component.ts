import { Component, OnInit, Input } from '@angular/core';
import { ProcrumentService } from 'src/app/service/procrument.service';
import { FilterComponent } from 'src/app/shared/filter/filter.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ProcurementchartdetailComponent } from 'src/app/shared/procurementchartdetail/procurementchartdetail.component';
import { ProcurementModel } from 'src/app/service/procurementmodel.service';

@Component({
  selector: 'app-chartnumberofsuppliers',
  templateUrl: './chartnumberofsuppliers.component.html',
  styleUrls: ['./chartnumberofsuppliers.component.css']
})
export class ChartnumberofsuppliersComponent implements OnInit {
  single: any[];
  numberofsupplier: any;
  @Input() type: string;
  @Input() chartModel: ProcurementModel;
  IsError: boolean;
  screenWidth: number;
  IsFiltered: boolean;
  IsLoading: boolean;
  constructor(private procrumentService : ProcrumentService, private dialog : MatDialog, private snackBar : MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
   }

  ngOnInit() {
    if(this.screenWidth < 600){
      this.view = [350, 400];
    }
   this.onGetNumberOfSupplier()
  }
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Number Of Active Supplier';

  colorScheme = {
    domain: ['#9a0007', '#4a0072', '#e64a19', '#003300', '#fbc02d', '#66bb6a']
  };

  onRefresh() {
    this.IsLoading = true;
    this.numberofsupplier = this.chartModel.numberOfActiveSupplier

    this.onLoadResetProp()
    this.procrumentService.GetNumberOfSupplier().subscribe(({ data, isFiltered }) => {
      this.numberofsupplier = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfActiveSupplier = this.numberofsupplier
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    this.procrumentService.GetChartDetail(this.type, chart.name).subscribe((data: any) => {
      this.dialog.open(ProcurementchartdetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter(){
    let dialog = this.dialog.open(FilterComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetNumberOfSupplier()
    })
  }
  private onLoadResetProp() : void {
    if (this.chartModel.numberOfActiveSupplier == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }
  
  onGetNumberOfSupplier() {
    this.numberofsupplier = this.chartModel.numberOfActiveSupplier

    this.onLoadResetProp()
    this.procrumentService.GetNumberOfSupplier().subscribe(({ data, isFiltered}) => {
      this.numberofsupplier = data.reverse()
      this.IsFiltered = isFiltered
      this.chartModel.numberOfActiveSupplier = this.numberofsupplier
      this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsLoading = false
    this.IsError = true
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
