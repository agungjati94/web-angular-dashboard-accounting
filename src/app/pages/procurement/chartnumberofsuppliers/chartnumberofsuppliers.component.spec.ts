import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartnumberofsuppliersComponent } from './chartnumberofsuppliers.component';

describe('ChartnumberofsuppliersComponent', () => {
  let component: ChartnumberofsuppliersComponent;
  let fixture: ComponentFixture<ChartnumberofsuppliersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartnumberofsuppliersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartnumberofsuppliersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
