import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ProcurementModel } from 'src/app/service/procurementmodel.service';

@Component({
  selector: 'app-procurement',
  templateUrl: './procurement.component.html',
  styleUrls: ['./procurement.component.css']
})
export class ProcurementComponent implements OnInit {
  constructor(public dialog: MatDialog, public cardModel: ProcurementModel) {
   
  }

  ngOnInit() {
   console.log("card ",    this.cardModel) 
  }
   
  
}
