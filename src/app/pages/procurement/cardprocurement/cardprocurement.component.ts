import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FilterComponent } from 'src/app/shared/filter/filter.component';
import { ProcurementdetailComponent } from 'src/app/shared/procurementdetail/procurementdetail.component';
import { ProcrumentService } from 'src/app/service/procrument.service';
import { ProcurementModel } from 'src/app/service/procurementmodel.service';

@Component({
  selector: 'app-cardprocurement',
  templateUrl: './cardprocurement.component.html',
  styleUrls: ['./cardprocurement.component.css']
})
export class CardprocurementComponent implements OnInit {

  @Input() count: number;
  @Input() description: string
  @Input() type: string
  @Input() cardModel: ProcurementModel
  IsLoading: boolean
  IsError : boolean
  IsFiltered : boolean;
  constructor(public dialog: MatDialog, private procurementService: ProcrumentService, private snackBar: MatSnackBar) {
    
   }

  ngOnInit() {
    if (this.type == "MPRNeedToConfirm") {
      this.count = this.cardModel.countMPRNeedToConfirm;
      this.callMPRNeedToConfirm();
    } else if (this.type == "MPRNeedToRunning") {
      this.count = this.cardModel.countMPRNeedToRunning;
      this.callMPRNeedToRunning();
    }
    else if (this.type == "OutstandingMPRAgainstPO") {
      this.count = this.cardModel.countOutstandingMPRAgainstPO;
      this.callOutstandingMPRAgainstPO();
    }
    else if (this.type == "OutstandingMPRAgainstGRN") {
      this.count = this.cardModel.countOutstandingMPRAgainstGRN;
      this.callOutstandingMPRAgainstGRN();
    }
    else if (this.type == "PONeedtoConfirm") {
      this.count = this.cardModel.countPONeedtoConfirm;
      this.callPONeedtoConfirm();
    }
    else if (this.type == "PONeedtoRunning") {
      this.count = this.cardModel.countPONeedtoRunning;
      this.callPONeedtoRunning()
    }
    else if (this.type == "OutstandingPOAgainstGRN") {
      this.count = this.cardModel.countOutstandingPOAgainstGRN;
      this.callOutstandingPOAgainstGRN();
    }
    else if (this.type == "OutstandingPOAgainstSI") {
      this.count = this.cardModel.countOutstandingPOAgainstSI;
      this.callOutstandingPOAgainstSI();
    }
  }

private onLoadResetProp() : void {
  this.IsError = false
  this.IsFiltered = false
}
  private callOutstandingPOAgainstSI() {
    if(this.cardModel.countOutstandingPOAgainstSI == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountPOOutstandingAgainstSI().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingPOAgainstSI = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callOutstandingPOAgainstGRN() {
    if(this.cardModel.countOutstandingPOAgainstGRN == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountPOOutstandingAgainstGRN().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingPOAgainstGRN = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callPONeedtoRunning() {
    if(this.cardModel.countPONeedtoRunning == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountPONeeedtoRunning().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countPONeedtoRunning = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callPONeedtoConfirm() {
    if(this.cardModel.countPONeedtoConfirm == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountPONeeedtoConfirm().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countPONeedtoConfirm = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callOutstandingMPRAgainstGRN() {
    if(this.cardModel.countOutstandingMPRAgainstGRN == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountMPROutstandingAgainstGRN().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingMPRAgainstGRN = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callOutstandingMPRAgainstPO() {
    if(this.cardModel.countOutstandingMPRAgainstPO == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountMPROutstandingAgainstPO().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countOutstandingMPRAgainstPO = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callMPRNeedToRunning() {
    if(this.cardModel.countMPRNeedToRunning == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountMPRNeeedtoRunning().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countMPRNeedToRunning = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  private callMPRNeedToConfirm() {
    if(this.cardModel.countMPRNeedToConfirm == 0) this.IsLoading = true
    this.onLoadResetProp()

    this.procurementService.CountMPRNeeedtoConfirm().subscribe(({ count, isFiltered }) => {
      this.IsLoading = false
      this.IsFiltered = isFiltered
      this.cardModel.countMPRNeedToConfirm = count;
      this.count = count
    }, error => {
      this.displayError(error);
    });
  }

  onRefresh() : void {
    this.IsLoading = true
    if (this.type == "MPRNeedToConfirm") {
      this.callMPRNeedToConfirm();
    } else if (this.type == "MPRNeedToRunning") {
      this.callMPRNeedToRunning();
    }
    else if (this.type == "OutstandingMPRAgainstPO") {
      this.callOutstandingMPRAgainstPO();
    }
    else if (this.type == "OutstandingMPRAgainstGRN") {
      this.callOutstandingMPRAgainstGRN();
    }
    else if (this.type == "PONeedtoConfirm") {
      this.callPONeedtoConfirm();
    }
    else if (this.type == "PONeedtoRunning") {
      this.callPONeedtoRunning()
    }
    else if (this.type == "OutstandingPOAgainstGRN") {
      this.callOutstandingPOAgainstGRN();
    }
    else if (this.type == "OutstandingPOAgainstSI") {
      this.callOutstandingPOAgainstSI();
    }
  }

  openFormFilter(): void {
    let dialog = this.dialog.open(FilterComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(refresh => {
      if(refresh) this.onRefresh()
    })
  }



  openTable(): void {
    this.procurementService.GetProcurementDetail(this.type).subscribe((data: any) => {
      this.dialog.open(ProcurementdetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  private displayError(error: any) {
    this.IsLoading = false
    this.IsError = true
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}
