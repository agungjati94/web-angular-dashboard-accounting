import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardprocurementComponent } from './cardprocurement.component';

describe('CardprocurementComponent', () => {
  let component: CardprocurementComponent;
  let fixture: ComponentFixture<CardprocurementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardprocurementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardprocurementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
