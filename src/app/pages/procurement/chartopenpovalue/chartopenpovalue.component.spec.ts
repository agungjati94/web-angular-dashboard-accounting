import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartopenpovalueComponent } from './chartopenpovalue.component';

describe('ChartopenpovalueComponent', () => {
  let component: ChartopenpovalueComponent;
  let fixture: ComponentFixture<ChartopenpovalueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartopenpovalueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartopenpovalueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
