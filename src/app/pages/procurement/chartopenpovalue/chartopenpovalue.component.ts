import { Component, OnInit, Input } from '@angular/core';
import { ProcrumentService } from 'src/app/service/procrument.service';
import { FilterComponent } from 'src/app/shared/filter/filter.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ProcurementchartdetailComponent } from 'src/app/shared/procurementchartdetail/procurementchartdetail.component';
import { ProcurementModel } from 'src/app/service/procurementmodel.service';

@Component({
  selector: 'app-chartopenpovalue',
  templateUrl: './chartopenpovalue.component.html',
  styleUrls: ['./chartopenpovalue.component.css']
})
export class ChartopenpovalueComponent implements OnInit {
  single: any[];
  openpovaluedata: any;
  @Input() type: string;
  @Input() chartModel: ProcurementModel
  IsError: boolean;
  screenWidth: number
  IsFiltered: boolean;
  IsLoading: boolean;
  constructor(private procrumentService : ProcrumentService, private dialog : MatDialog, private snackBar : MatSnackBar) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  ngOnInit() {
    if(this.screenWidth < 600){
      this.view = [350, 400];
    }
    this.onGetOpenPOValue()
  }
  view: any[] = [700, 400];

  
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showXAxisLabel = true;
  xAxisLabel = 'Bulan';
  showYAxisLabel = true;
  yAxisLabel = 'Open PO Value';

  colorScheme = {
    domain: ['#e6ee9c', '#ffe082', '#d32f2f', '#64b5f6', '#4db6ac', '#66bb6a']
  };

  onRefresh(){
    this.IsLoading = true;
    this.openpovaluedata = this.chartModel.openPOValue

    this.onLoadResetProp()
    this.procrumentService.GetOpenPOValue().subscribe(({ data, isFiltered}) => {
      this.openpovaluedata = data
      this.IsFiltered = isFiltered
      this.chartModel.openPOValue = data
    this.IsLoading = false
    }, error => {
      this.displayError(error)
    })
  }

  onSelect(chart) {
    console.log(chart)
    this.procrumentService.GetChartDetailOpenPOValue(this.type, chart.series, chart.currencyId).subscribe((data: any) => {
      this.dialog.open(ProcurementchartdetailComponent, {
        width: '85vw',
        height: '90vh',
        data: { data: data, type: this.type }
      });
    }, error => {
      this.displayError(error);
    })
  }

  openFormFilter(){
    let dialog = this.dialog.open(FilterComponent, {
      width: '300px',
      data: {
        type: this.type
      }
    });

    dialog.afterClosed().subscribe(result => {
      this.onGetOpenPOValue()
    })
  }

  private onLoadResetProp() : void {
    if(this.chartModel.openPOValue == null) this.IsLoading = true

    this.IsError = false
    this.IsFiltered = false
  }
  
  onGetOpenPOValue() {
    this.openpovaluedata = this.chartModel.openPOValue
    
  this.onLoadResetProp()
    this.procrumentService.GetOpenPOValue().subscribe(({ data, isFiltered}) => {
      this.openpovaluedata = data
      this.IsFiltered = isFiltered
      this.chartModel.openPOValue = data
    this.IsLoading = false

    }, error => {
      this.displayError(error)
    })
  }

  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false
    this.snackBar.open("An error occured in server  : " + error && error.error && error.error.Message, "X", {
      verticalPosition: 'top'
    });
  }
}

