import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockmonitoringComponent } from './stockmonitoring.component';

describe('StockmonitoringComponent', () => {
  let component: StockmonitoringComponent;
  let fixture: ComponentFixture<StockmonitoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockmonitoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockmonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
