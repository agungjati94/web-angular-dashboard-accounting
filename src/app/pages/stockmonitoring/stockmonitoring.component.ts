import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DashboardService } from 'src/app/service/dashboard.service';
import { StockMonitoringModel } from 'src/app/model/stockmonitoring.model';
import { MatTableDataSource, MatSnackBar, MatSort } from '@angular/material';
import { FormControl } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { tap, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-stockmonitoring',
  templateUrl: './stockmonitoring.component.html',
  styleUrls: ['./stockmonitoring.component.css']
})
export class StockmonitoringComponent implements OnInit {
  displayedColumns: string[] = [
    'partCD',  'partname', 'groupingCD', 'unitCD'
     , 'invQty', 'bookQty', 'prodAvailable', 'orderQty',  'stockExpected', 'safetystock', 'minimumorder', 'sugestionqtytoorder', 
    // // 'maxstock',  
        'vendorCD',  'divisionCD', 'departmentCD', 'projectCD', 'priority', 'warehouseCD', 'partCat1', 'partCat2', 'partCat3' //'more'
  ];
  dataSource = new MatTableDataSource<any>();
  IsError: boolean;
  IsLoading: boolean;
  isLoadingPart: boolean;
  isLoadingSupplier: boolean;
  departments: any[];
  divisions: any[];
  projects: any[];
  partGroups: any[];
  partCats: any[];
  suppliers: any;
  priorities  :any[];
  itemPart: any;
  warehouses: any;
  model : StockMonitoringModel = new StockMonitoringModel()
  @ViewChild(MatSort) sort: MatSort;
  todayControl = new FormControl(new Date());
  today = this.todayControl.value
  @ViewChild('name') partField: ElementRef;
  @ViewChild('supplier') supplierField: ElementRef;
  

  constructor(private service: DashboardService, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.initializedLoad();
  }

  private initializedLoad() {
    this.service.getDepartment().subscribe(data => {
      this.departments = data;
    }, error => {
      this.displayError(error);
    });
    this.service.getDivision().subscribe(data => {
      this.divisions = data;
    }, error => {
      this.displayError(error);
    });
    this.service.getProject().subscribe(data => {
      this.projects = data;
    }, error => {
      this.displayError(error);
    });
    this.service.getPartGroup().subscribe(data => {
      this.partGroups = data;
    }, error => {
      this.displayError(error);
    });
    this.service.getPartCat().subscribe(data => {
      this.partCats = data;
    }, error => {
      this.displayError(error);
    });
    this.service.getSupplier().subscribe(data => {
      this.suppliers = data;
    }, error => {
      this.displayError(error);
    });
    this.service.getPriority().subscribe(data => {
      this.priorities = data;
    }, error => {
      this.displayError(error);
    });
    this.service.getWarehouse().subscribe(data => {
      this.warehouses = data;
    }, error => {
      this.displayError(error);
    });

    this.model.Page = 1
    this.model.Limit = 50

    this.GetStockMonitoring();
  }

  ngAfterViewInit() {
    fromEvent(this.partField.nativeElement, 'keypress')
      .pipe(tap(x => this.isLoadingPart = true))
      .pipe(debounceTime(500))
      .subscribe(keyboardEvent => {
        this.service.getItemPart(keyboardEvent['target'].value.trim().toLowerCase()).subscribe(data => {
          this.isLoadingPart = false;
          this.itemPart = data;
        }, error => {
          this.isLoadingPart = false;
          this.snackbar.open(error.error.Message || "Sorry, on error ocured", "X", {
            duration: 5000,
            verticalPosition: 'top'
          })
        })
      })

      fromEvent(this.supplierField.nativeElement, 'keypress')
        .pipe(tap(x => this.isLoadingSupplier = true))
        .pipe(debounceTime(500))
        .subscribe(keyboardEvent => {
          this.service.getSupplierBySearch(keyboardEvent['target'].value.trim().toLowerCase()).subscribe(data => {
            this.isLoadingSupplier = false;
            this.suppliers = data;
          }, error => {
            this.isLoadingSupplier = false;
            this.snackbar.open(error.error.Message || "Sorry, on error ocured", "X", {
              duration: 5000,
              verticalPosition: 'top'
            })
          });
        });
  }

  GetStockMonitoring(){
    this.IsLoading = true
    this.service.getStockMonitoring(this.model).subscribe( ({report,count}) => {
      this.dataSource = new MatTableDataSource<any>(report);
      this.model.TotalRow = count;
      this.dataSource.sort = this.sort
      this.IsLoading = false
    }, error => {
      this.displayError(error);
    });
  }

  onMovePage(e){
    this.IsLoading = true
    this.model.Page = ++e.pageIndex
    this.service.getStockMonitoring(this.model).subscribe(({report,count}) => {
      this.dataSource = new MatTableDataSource<any>(report);
      this.model.TotalRow = count;
      this.IsLoading = false
    }, error => {
      this.displayError(error);
    })
  }
  changeTransDate(){
    this.today =  this.model.TransDate == null ? new Date : this.model.TransDate;
  }
  private displayError(error: any) {
    this.IsError = true
    this.IsLoading = false

    this.snackbar.open(error.error.Message || "Sorry, on error ocured", "X", {
      verticalPosition: 'top'
    });
  }

}
