import {
    animation, trigger, animateChild, group,
    transition, animate, style, query
  } from '@angular/animations';
  
  export const transAnimation = animation([
    style({
      height: '{{ height }}',
      opacity: '{{ opacity }}',
      backgroundColor: '{{ backgroundColor }}'
    }),
    animate('{{ time }}')
  ]);
  
  // Routable animations
  export const slideInAnimation =
    trigger('routeAnimations', [
        transition('* <=> AboutPage', [
            query(':enter', style({ opacity: 0 })),
            query(':leave', [
              animate('1s', style({ opacity: 0 }))
            ]),
            query(':enter', [
              animate('1s', style({ opacity: 1 }))
            ]),
          ])
        ])